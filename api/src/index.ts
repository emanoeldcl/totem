import cors from "cors";
import express from "express";
import bodyParser from "body-parser";
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();
const app = express();

app.use(bodyParser.json());
app.use(cors());

app.get("/institution", async (req, res) => {
  res.json(
    await prisma.institution.findFirst({
      include: {
        items: {
          where: {
            parentId: null,
          },
        },
      },
    })
  );
});

app.get("/items/:id(\\d+)", async (req, res) => {
  res.json(
    await prisma.item.findOne({
      where: {
        id: parseInt(req.params.id, 10),
      },
      select: {
        id: true,
        title: true,
        parentId: true,
        content: true,
        children: {
          select: {
            id: true,
            title: true,
            style: true,
          },
        },
      },
    })
  );
});

app.listen(3001, () =>
  console.log("🚀 Server ready at: http://localhost:3001")
);
