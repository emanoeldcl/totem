# API do projeto totem

```bash
npm run setup # para iniciar o banco de dados
npm run studio # para abrir a interface do banco de dados
npm start # para rodar a api
```

## Como modificar o banco de dados

Faça a mudança no arquivo ./prisma/schema.prisma de acordo com a [documentação](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-schema).

Logo então execute o comando:

```bash
npm run migrate # vai salvar as alterações no banco de dados
```
