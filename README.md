# totem

## Instalando

```sh
cd api
npm install
cd ../app
npm install
cp .env.example .env
```

## Executando

```sh
# na raiz do repositório
npm start
```
