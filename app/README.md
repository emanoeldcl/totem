# Totem

## Como rodar o frontend
Entre até o diretório '\app' e rode:
```
npm install
```
 
### Compilar e rodar o projeto
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
