import Vue from 'vue';
import VueRouter from 'vue-router';
import Router from 'vue-router';
import WelcomePage from '../pages/WelcomePage';
import StudentPage from '../pages/StudentPage';
import VisitorPage from '../pages/VisitorPage';
import ItemPage from '../pages/ItemPage';

Vue.use(VueRouter);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Welcome',
      component: WelcomePage
    },
    {
      path: '/estudante',
      name: 'Student',
      component: StudentPage
    },
    {
      path: '/visitante',
      name: 'Visitor',
      component: VisitorPage
    },
    {
      path: '/item/:id',
      component: ItemPage,
    }
  ]
});

export default router;
