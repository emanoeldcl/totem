import axios from 'axios'

const BASE_PATH = process.env.VUE_APP_BASE_PATH;

export default axios.create({
  baseURL: BASE_PATH
});
